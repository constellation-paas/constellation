# Hashistack-in-a-box

This role install an hashistack on a machine.

To test it with Vagrant you can go in the `vagrant` folder and run:
```
$ make deps
```

Then launch the vagrant VM:
```
$ vagrant up
```

Enjoy Nomad + Consul:
```
$ vagrant ssh
$ nomad server members
$ nomad node status
```

If you use this role on your own you must provide your own Nomad, Consul, Vault
binary, or used the makefile provided in the `vagrant` folder.

## Caveats

- No network segmentation: all the docker container runs in the same network.
  Any container can access any other container.
- Putting password anywhere is insecure: Until Vault will come along, any
  password is in cleartext.

## Development steps

### 0.1.0

- [X] Raw API access for job processing
- [X] Able to run a static website with https (let's encrypt staging) locally

### 0.2.0

- [X] Can be deployed on a real remote server
- [X] API access limited to localhost
- [X] Host static website with HTTPS in the InterWebz
- [ ] Allow local HTTPS via the staging server of Let's Encrypt

### 0.3.0

- [X] Terraform configuration for the services and operator
- [ ] Database support as a service of the PaaS

### 0.4.0

- [ ] Vault added in the stack
- [ ] Push all tokens to Vault
- [ ] Vault used for internal PKI, and bootstrap problem solved
- [ ] Vault is used for the database users

### 0.5.0

- [ ] Integrated monitoring via Prometheus & Grafana
- [ ] Integrated log processing via external ELK stack

### 0.6.0

- [ ] Better ACL for Nomad/Consul/Vault to prevent unauthorized access
- [ ] Nomad + Consul UI as part of the stack

### 0.7.0

- [ ] Heroku like deployment on a Git push

### 1.0.0

- [ ] S3 compatible storage
- [ ] Shared filesystem based on Ceph/Gluster
- [ ] Network & service segmentation via docker networks and Consul connect

### 2.0.0

- [ ] User friendly interface, with application catalog
- [ ] High level API layer, abstracting NCVT for end-user
- [ ] Simple configuration format so user don't need to know NCVT to deploy their projects
