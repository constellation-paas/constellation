#!/usr/bin/env zsh

# Check the status of Nomad and Consul. Both should be up
echo "Nomad status"
nomad status

echo ""
echo "Consul members"
consul members
