# Defining docker networks
# ------------------------

# This is our global network
resource "docker_network" "hashistack_network_global" {
  name = "hashistack-global"
}

resource "nomad_job" "hashistack_system_proxy" {
  jobspec = "${file("${path.module}/nomad-jobs/traefik-proxy.nomad")}"
}
