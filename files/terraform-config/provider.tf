# Docker provider
provider "docker" {
  host = "unix:///run/docker.sock"
}

# Nomad provider
provider "nomad" {
  address = "http://127.0.0.1:4646"
  version = "~> 1.4"
}
